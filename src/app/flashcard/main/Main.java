/**
 * 
 */
package app.flashcard.main;

/**
 * Main method that instantiates the MainMenu and coupled ActionSwing objects
 * @author Daniel Latham
 * @version 0.0.1
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MainMenu menu = new MainMenu();
		ActionSwing actions = new ActionSwing();
		menu.setActionObject(actions);
	}

}
