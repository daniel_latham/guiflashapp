package app.flashcard.main;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.regex.*;

import javax.imageio.ImageIO;
import javax.swing.*;
/**
 * Frame for iterating over cards
 * @author dan
 *
 */

@SuppressWarnings("serial")
public class CardFrame extends JFrame 
{

	/**
	 * Constructor for the practice frame
	 * @param title
	 * @param mode
	 * @param currentSet
	 * @throws HeadlessException
	 */
	protected CardFrame(String title, int mode, FlashSet currentSet) throws HeadlessException 
	{
		this.setTitle(title);
		this.mode = mode;
		this.currentSet = currentSet;
		Dimension d = new Dimension(this.prefX,this.prefY);
		this.setPreferredSize(d);
		this.actions = new ActionFrame();
		setPracticeKeyPanel();
		///Outline for basic frame, should have places for front and back sides and buttons
		if (this.mode == this.LEITNER)
		{
			leitnerInit();
		}
		else if (this.mode == this.DRILL)
		{
			drillInit();
		}
	}
	/**
	 * Initializes the leitner system
	 */
	private void leitnerInit()
	{
		setLeitnerPoolAndCard();	
		initHelper();
		
	}
	/**
	 * Initializes the drill system
	 */
	private void drillInit()
	{
		this.drillPool = this.currentSet.returnPool(0);
		setDrillCard();
		initHelper();
	}
	/**
	 * Initialization helper method for above drill and leitner systems
	 */
	@SuppressWarnings("deprecation")
	private void initHelper()
	{
		this.add(keyPanel, BorderLayout.SOUTH);
		if (!isImage)
		{
			roll();
			this.flashcardInterface.setLayout(new BorderLayout());
			this.flashcardInterface.add(this.question, BorderLayout.CENTER);
			this.flashcardInterface.add(this.answer, BorderLayout.SOUTH);
			
			this.add(flashcardInterface);
		}
		setWindowResizeListener();
		setMouseListener();
		this.pack();
		this.show(true);
	}
	/**
	 * Adds resize window listener for when this.ismage image
	 */
	private void setWindowResizeListener()
	{
		this.addComponentListener( new ComponentListener(){

			@Override
			public void componentResized(ComponentEvent e) {
				if(isImage)
				{
					resizeImageToWindow();
				}
			}

			@Override
			public void componentMoved(ComponentEvent e) {
				
				
			}

			@Override
			public void componentShown(ComponentEvent e) {
				
				
			}

			@Override
			public void componentHidden(ComponentEvent e) {
				
				
			}


		});
	}
	/**
	 * Resizes image
	 */
	private void resizeImageToWindow()
	{
		
		int x = this.getWidth();
		int y = this.getHeight();
		BufferedImage tmpimg = null;
		tmpimg = resize(this.img,x,y);
		this.qPicture.setIcon(new ImageIcon(tmpimg));
		
		//Save size for later
		this.prefX = x;
		this.prefY = y;
		Dimension d = new Dimension(this.prefX,this.prefY);
		this.setPreferredSize(d);
		
	}
	/**
	 * Sets mouselistener
	 */
	private void setMouseListener()
	{
		this.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {
				
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				mouseInitPos = getMousePosition();
				
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				mouseExitPos = getMousePosition();
				if (mouseInitPos != null && mouseExitPos != null)
				{
					//Check if mouse Init was in middle 1/5 of width
					if(mouseInitPos.getX() >= (prefX/10)*4 && mouseInitPos.getX() <= (prefX/10)*6)
					{
						//Check Exit click side, decide then what to do
						if(mouseExitPos.getX() >= 0 && mouseExitPos.getX() < (prefX/10)*4)
						{
							//DO NOT KNOW choice
							//switch for mode
							if (mode == LEITNER)
							{
								actions.leitnerLoop("Do Not Know");
							}
							else if (mode == DRILL)
							{
								actions.drillLoop("Do Not Know");
							}
						}
						else if (mouseExitPos.getX() > (prefX/10)*6 && mouseExitPos.getX() <= prefX)
						{
							//I KNOW choice
							//switch for mode
							if (mode == LEITNER)
							{
								actions.leitnerLoop("I Know");
							}
							else if (mode == DRILL)
							{
								actions.drillLoop("I Know");
							}
						}
					}
				}
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				
				
			}
			
		});
		this.addMouseMotionListener(new MouseMotionListener(){

			@Override
			public void mouseDragged(MouseEvent e) {
				
				
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				
			}
			
		});
	}

	/**
	 * Sets up the 4 key options
	 */
	private void setPracticeKeyPanel()
	{
		keyPanel.setLayout(new GridLayout(1,4));
		String[] keys = {"Do Not Know","Flip Card","I Know", "Exit"};
		for (int i = 0; i < keys.length; i++)
		{
			final String label = keys[i];
			JButton keyButton = new JButton(label);
			keyButton.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					actions.choose(label);
				}
			});
			keyPanel.add(keyButton);
		}
	}
	/**
	 * Adds continue keyPanel to the this frame, packs, and repaints
	 */
	private void addContinueKeyPanel()
	{
		this.tempKeyPanel = new JPanel();
		this.tempKeyPanel.setLayout(new GridLayout(1,1));
		final String label = "Continue";
		JButton keyButton = new JButton(label);
		keyButton.addActionListener(new
		ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				actions.choose(label);
			}
		});
		this.tempKeyPanel.add(keyButton);
		add(this.tempKeyPanel, BorderLayout.SOUTH);
		pack();
		repaint();
	}
	/**
	 * Randomly picks (head or tails style) to show the front or back of the card
	 */
	private void roll()
	{
		if (!isImage)
		{
			double choice = Math.random();
			if (choice >= .5)
			{
				question.setText(front);
				question.setHorizontalAlignment(SwingConstants.CENTER);
				answer.setText("");
			}
			else
			{
				question.setText(back);
				question.setHorizontalAlignment(SwingConstants.CENTER);
				answer.setText("");
			}
			repaint();
		}
	}
	/**
	 * Flips the shown card
	 */
	private void flip()
	{
		if (!isImage)
		{
			if (question.getText().equals(back))
			{
				question.setText(front);
				question.setHorizontalAlignment(SwingConstants.CENTER);
				answer.setText("");
			}
			else
			{
				question.setText(back);
				question.setHorizontalAlignment(SwingConstants.CENTER);
				answer.setText("");
			}
			repaint();
		}
	}
	/**
	 * Exits this Frame object, but not the program
	 */
	private void exit()
	{
		WindowEvent windowClosing = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
		this.dispatchEvent(windowClosing);
	}
	/**
	 * Shows both of the cards
	 */
	private void showBothCards()
	{
		if (!isImage)
		{
			if (question.getText().equals(back))
			{
				question.setText(back);
				question.setHorizontalAlignment(SwingConstants.CENTER);
				answer.setText(front);
			}
			else
			{
				question.setText(front);
				question.setHorizontalAlignment(SwingConstants.CENTER);
				answer.setText(back);
			}
		}
		else if(isImage)
		{
			if (isPicturePath(this.front))
			{
				answer.setText(this.back);
			}
			else
			{
				answer.setText(this.front);
			}	
		}
		repaint();
	}
	/**
	 * Checks if the entered option is equal to actual card
	 * @return
	 */
	private boolean checkAnswer()
	{
		String tempAns = this.answer.getText();
		boolean correct;
		///Switch for front/back
		if (!isImage)
		{
			if (question.getText().equals(this.back))
			{
				///check front == tempAns
				correct = tempAns.equalsIgnoreCase(this.front);

			}
			else
			{
				///Check back == tempAns
				correct = tempAns.equalsIgnoreCase(this.back);
			}
		}
		else
		{
			if (isPicturePath(this.front))
			{
				correct = tempAns.equalsIgnoreCase(this.back);
			}
			else
			{
				correct = tempAns.equalsIgnoreCase(this.front);
			}
			
		}
		
		if (correct)
		{
			JOptionPane.showMessageDialog(null, "Right answer.", "Congratulations", JOptionPane.INFORMATION_MESSAGE);
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Wrong answer.", "ERROR", JOptionPane.INFORMATION_MESSAGE);
		}
		
		return correct;
	}
	/**
	 * Prods the currentFlashSet for a viable pool with exponential favor toward
	 * the first pool and so on and picks a card at random
	 */
	private void setLeitnerPoolAndCard()
	{
		this.poolNum = currentSet.pickPool();
		String[] card = currentSet.prodPool(poolNum-1);
		this.front = card[0];
		this.back = card[1];
		if (isPicturePath(this.front))
		{
			this.isImage = true;
			removeJQuestion();
			addImage();
		}
		else if (isPicturePath(this.back))
		{
			this.isImage = true;
			removeJQuestion();
			addImage();
		}
		else
		{
			roll();
		}
		this.cardNum = Integer.parseInt(card[2]);
	}
	/**
	 * Chooses a card for the drill
	 */
	@SuppressWarnings("deprecation")
	private void setDrillCard()
	{
		String[] card = this.drillPool.pickCard();
		///In case of null values
		if (card[0] == null || card[1] == null || card[2] == null)
		{
			JOptionPane.showMessageDialog(null, "No cards in Pool #1. Reload set or edit cards from main menu", "ERROR", JOptionPane.INFORMATION_MESSAGE);
			exit();
		}
		else
		{
			this.front = card[0];
			this.back = card[1];
			this.cardNum = Integer.parseInt(card[2]);
			
			if (isPicturePath(this.front))
			{
				this.isImage = true;
				removeJQuestion();
				addImage();
			}
			else if (isPicturePath(this.back))
			{
				this.isImage = true;
				removeJQuestion();
				addImage();
			}
			else
			{
				roll();
			}
			this.pack();
			this.show(true);
		}
	}
	/**
	 * Checks if side of card is or is not a picture, returns boolean
	 * @return boolean Is or is not a picture
	 */
	private boolean isPicturePath(String side)
	{
		String regex = "([0-9a-zA-Z_\\-/]*\\.jpg)||([0-9a-zA-Z_\\-/]*\\.jpeg)||([0-9a-zA-Z_\\-/]*\\.png)";
		Pattern picPattern = Pattern.compile(regex);
		Matcher sMatcher = picPattern.matcher(side);
		if (sMatcher.matches())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	/**
	 * addsQuestion(text)and answer labels to frame
	 */
	private void addJQuestion()
	{
		this.flashcardInterface = new JPanel();
		this.flashcardInterface.setLayout(new BorderLayout());
		this.flashcardInterface.add(this.question, BorderLayout.CENTER);
		this.flashcardInterface.add(this.answer, BorderLayout.SOUTH);
		
		this.add(flashcardInterface);
		pack();
		repaint();
	}
	/**
	 * Removes question(text or picture) and answer from frame
	 */
	private void removeJQuestion()
	{
		this.remove(flashcardInterface);
		pack();
		repaint();
	}
	/**
	 * Returns scaled buffered image
	 * @param image
	 * @param width
	 * @param height
	 * @return BufferedImage
	 */
	private BufferedImage resize(BufferedImage image, int width, int height) {
	    BufferedImage bi = new BufferedImage(width, height, BufferedImage.TRANSLUCENT);
	    Graphics2D g2d = (Graphics2D) bi.createGraphics();
	    g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
	    g2d.drawImage(image, 0, 0, width, height, null);
	    g2d.dispose();
	    return bi;
	}
	/**
	 * Overrides the "roll" method, and makes the "flip" method obsolete for this
	 * flash card
	 * 
	 */
	private void addImage()
	{
		if(isPicturePath(this.front))
		{
			this.flashcardInterface = new JPanel();
			this.flashcardInterface.setLayout(new BorderLayout());
			this.qPicture = new JLabel();
			
			this.img = null;
			File file = new File(this.back);
			try
			{
				this.img = ImageIO.read(file);
			} catch (Exception e){System.out.println(e);}
			this.img = resize(this.img,this.prefX,this.prefY);
			
			this.qPicture.setIcon(new ImageIcon(this.img));
			
			
			this.answer = new JTextArea();
			this.answer.setText("");
			//@Debug
			//this.question.setText(this.front);
			this.flashcardInterface.add(this.qPicture, BorderLayout.CENTER);
			this.flashcardInterface.add(this.answer, BorderLayout.SOUTH);
			this.add(flashcardInterface);
		}
		else if (isPicturePath(this.back))
		{
			this.flashcardInterface = new JPanel();
			this.flashcardInterface.setLayout(new BorderLayout());
			this.qPicture = new JLabel();
			
			this.img = null;
			File file = new File(this.back);
			try
			{
				this.img = ImageIO.read(file);
			} catch (Exception e){System.out.println(e);}
			BufferedImage tempimg = resize(this.img,this.prefX,this.prefY);
			
			this.qPicture.setIcon(new ImageIcon(tempimg));
			
			this.answer = new JTextArea();
			this.answer.setText("");
			//@Debug
			//this.question.setText(this.back);
			this.flashcardInterface.add(this.qPicture, BorderLayout.CENTER);
			this.flashcardInterface.add(this.answer, BorderLayout.SOUTH);
			this.add(flashcardInterface);
		}
		pack();
		repaint();
		
	}
	/**
	 * Removes the flashcard interface, both sides of the card
	 */
	private void removeImage()
	{
		this.remove(flashcardInterface);
		pack();
		repaint();
	}
	/**
	 * Action frame class, decides on actions for above loops
	 * @author Daniel Latham
	 *
	 */
	private class ActionFrame {

		/**
		 * Constructor
		 */
		private ActionFrame() 
		{
			//Nothing
		}

		/**
		 * Depending on the current mode, when a button is pressed it decides what to do
		 * @param label
		 */
		private void choose(String label)
		{
			if (mode == LEITNER)
			{
				leitnerLoop(label);
			}
			else if (mode == DRILL)
			{
				drillLoop(label);
			}
		}
		/**
		 * Loop for the Leitner input
		 * @param label
		 */
		private void leitnerLoop(String label)
		{
			if (label.equalsIgnoreCase("I know"))
			{
				boolean tempCorrect = checkAnswer();
				if (tempCorrect)
				{
					currentSet.moveCard(poolNum, poolNum+1, cardNum);

				}
				if (isImage)
				{
					isImage = false;
					removeImage();
					addJQuestion();
				}
				
				setLeitnerPoolAndCard();
				
				//Repaints cards
			}
			else if (label.equalsIgnoreCase("Exit"))
			{
				exit();
			}
			else if (label.equalsIgnoreCase("Flip Card"))
			{
				flip();
			}
			else if (label.equalsIgnoreCase("Do Not Know"))
			{
				showBothCards();
				//Remove buttonSet
				remove(keyPanel);
				//Add continue button
				addContinueKeyPanel();
			}
			else if (label.equalsIgnoreCase("Continue"))
			{
				remove(tempKeyPanel);
				add(keyPanel, BorderLayout.SOUTH);
				if (isImage)
				{
					isImage = false;
					removeImage();
					addJQuestion();
				}
				setLeitnerPoolAndCard();
				roll();
			}

		}
		/**
		 * Loop for the Drill input
		 * @param label
		 */
		private void drillLoop(String label)
		{
			if (label.equalsIgnoreCase("I know"))
			{
				
				try
				{
					boolean tempCorrect = checkAnswer();
					if (tempCorrect)
					{
						drillPool.removeCard(cardNum);
					}
					if (isImage)
					{
						isImage = false;
						removeImage();
						addJQuestion();
					}
					setDrillCard();
					//Repaints cards
					roll();
				}
				catch (Exception e)
				{
					JOptionPane.showMessageDialog(null, "You have finished this study session!", "Congratulations!", JOptionPane.INFORMATION_MESSAGE);
					exit();
				}
			}
			else if (label.equalsIgnoreCase("Exit"))
			{
				exit();
			}
			else if (label.equalsIgnoreCase("Flip Card"))
			{
				flip();
			}
			else if (label.equalsIgnoreCase("Do Not Know"))
			{
				showBothCards();
				//Remove buttonSet, add other keyPanel
				remove(keyPanel);
				//Add continue button
				addContinueKeyPanel();
			}
			else if (label.equalsIgnoreCase("Continue"))
			{
				remove(tempKeyPanel);
				add(keyPanel, BorderLayout.SOUTH);
				setDrillCard();
				roll();
			}
		}
	}
	private int poolNum;
	private int cardNum;
	private boolean isImage = false;
	//Size of frame stuff for resizing picture
	private int prefX = 500;
	private int prefY = 400;
	//Mouse alignment stuff
	private Point mouseInitPos;
	private Point mouseExitPos;
	
	//Image and flashcard stuff
	private BufferedImage img;
	private JLabel qPicture;
	private JLabel question = new JLabel();
	private JTextArea answer = new JTextArea();
	
	//Front and back Strings of picked(currently shown) card
	private String front;
	private String back;
	
	private JPanel tempKeyPanel;
	private Pool drillPool;
	
	//All panels on this Frame
	private JPanel flashcardInterface = new JPanel();
	final private JPanel keyPanel = new JPanel();
	final private FlashSet currentSet;
	
	//Object coupled with CardFrame to take button-press input from
	//user
	final private ActionFrame actions;
	
	//Switches for MODE of this Frame
	final private int mode;
	final private int LEITNER = 0;
	final private int DRILL = 1;
}
