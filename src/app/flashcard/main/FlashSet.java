/*
 * FlashSet.java
 * 
 * Copyright 2014 Daniel Latham <l3li3l@torguard.tg>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General protected License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General protected License for more details.
 * 
 * You should have received a copy of the GNU General protected License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
package app.flashcard.main;

import java.io.File;

import java.util.Scanner;
import java.io.PrintWriter;

public class FlashSet 
{
	/** Costructor for new FlashSet using the FlashSet creator method in Pool1
	 * @param void
	 * @return void
	 */
	protected FlashSet()
	{
		createPools(false);
	}
	/** Constructor method for this FlashSet using the import command
	 * @param String Name of file you want to import
	 * @return void
	 */
	protected FlashSet(String filename)
	{
		//System.out.println("Importing file " + filename + "...");
		try
		{
			createPools(false);
			File file = new File(filename);
			if (file.exists())
			{
				try
				{
					Scanner scanner = new Scanner(file);
					while (scanner.hasNextLine())
					{
						//iterate forward once more
						String num = scanner.nextLine(); //CAN BE null(""), and if so, goes to pool 1
						String front = scanner.nextLine();
						String back = scanner.nextLine();
						int i = 0;
						try 
						{
							i = Integer.parseInt(num);
						} catch (Exception e)
						{
							//Nothing
						}
						Card instance = new Card(front,back);
						pools[i].addCard(instance);
					}
					scanner.close();
				}
				catch (Exception e)
				{
					System.out.println(e);
				}
				Thread.sleep(20);
			}
			else
			{
				Exception e = new Exception("File not found!");
				throw e;
			}
		}
		catch (Exception e) {/*System.out.println("File not found!");*/}
	}
	/** Returns the length of the the pool specified.
	 * @param int Pool number 0-4 inclusive
	 * @return int Number of elements, 0-* inclusive
	 */
	protected int getLength(int whichPool)
	{
		return pools[whichPool].getLength();
	}
	/**
	 * Returns deepcopy of specified pool by calling on helper method 
	 * returnPool.
	 * @param poolnum
	 * @return Pool
	 */
	protected Pool returnPool(int poolnum)
	{
		assert (0 <= poolnum && poolnum <= 4) : "Pool must be in range inclusive [0,4]";
		return copyPool(poolnum);
	}
	/** Returns the front and back of specified pool
	 * @param int Pool number 0-4 inclusive
	 * @return String[] Array of the 'front[0]' and 'back[1]' sides of card
	 */
	protected String[] prodPool(int poolnum)
	{
		return pools[poolnum].pickCard();
	}
	/** Method that picks a pool at random, with exponential increments
	 * favoring pool 1(0) which is half the space and checks the length 
	 * of each pool to make sure it turns out right, otherwise it loops again
	 * @param void
	 * @return int Number of selected pool, chosen generally within a couple iterations,
	 * the only drawback is that it can iterate 30-40 times before getting 
	 * to pool 5 if all cards are placed in pool 5.
	 */
	protected int pickPool()
	{
		//Need to make this so that pool1 is twice as likely as pool2, and so on
		int whichpool = 0;
		boolean stopswitch = false;
		boolean initPools1 = pool1.getLength() >= 1;
		boolean initPools2 = pool2.getLength() >= 1;
		boolean initPools3 = pool3.getLength() >= 1;
		boolean initPools4 = pool4.getLength() >= 1;
		boolean initPools5 = pool5.getLength() >= 1;
		while (!stopswitch)
		{
			double which = Math.random()*32;
			if (which >= 16 && initPools1)
			{
				whichpool = 1;
				stopswitch = true;
			}
			else if (which >= 8 && initPools2)
			{
				whichpool = 2;
				stopswitch = true;
			}
			else if (which >= 4 && initPools3)
			{
				whichpool = 3;
				stopswitch = true;
			}
			else if (which >= 2 && initPools4)
			{
				whichpool = 4;
				stopswitch = true;
			}
			else if (which >= 1 && initPools5)
			{
				whichpool = 5;
				stopswitch = true;
			}
		}
		return whichpool;
	}
	/** Helper method for constructing pools.
	 * @param boolean True if you are trying to enter cards in through command line
	 * and not with pre-written file.
	 * @return void
	 */
	protected void createPools(boolean oneOff)
	{
		pool1 = new Pool();
		pool2 = new Pool();
		pool3 = new Pool();
		pool4 = new Pool();
		pool5 = new Pool();
		pools = new Pool[5];// {pool1, pool2, pool3, pool4, pool5};
		
		pools[0] = pool1;
		pools[1] = pool2;
		pools[2] = pool3;
		pools[3] = pool4;
		pools[4] = pool5;
	}
	/** Writes current set to disk using the same notation as importing, 
	 * except this saves the pool number.
	 * @param void
	 * @return void
	 */
	protected void saveSet(String filePath)
	{
		PrintWriter kWriter;
		try
		{
			kWriter = new PrintWriter(filePath, "UTF-8");
		}
		catch (Exception e)
		{
			kWriter = null;
		}
		for (int i = 0; i < pools.length; i++)
		{
			if (pools[i].getLength() >= 1)
			{
				for (int j = pools[i].getLength() -1; j >= 0; j--)
				{
					Card temp = pools[i].removeCard(j);
					kWriter.println(i);
					kWriter.println(temp.getFront());
					kWriter.println(temp.getBack());
				}
			}
			else
			{
				//Nothing
			}
		}
		kWriter.close();
	}
	/**Moves card from one pool to another pool
	 * @param int Pool number the card you want to move is currently located.
	 * @param int Pool number of the destination.
	 * @param int Number of the card in the ArrayList
	 * @return void
	 */
	protected void moveCard(int poolInitNum, int poolFinalNum, int cardNum)
	{
		try
		{
			if (poolFinalNum <= 5)
			{
				Card returned = pools[poolInitNum-1].removeCard(cardNum);
				pools[poolFinalNum-1].addCard(returned);
			}
			///Does nothing if bigger than 5 because pool is already at #5 which is the highest one
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
	}
	/**
	 * Returns a deepcopy of pool #1
	 * @param poolnum
	 * @return Pool
	 */
	private Pool copyPool(int poolnum)
	{
		Pool tempRef = pools[poolnum];
		Pool returnedVal = new Pool();
		for (int i = 0; i < tempRef.getLength(); i++)
		{
			String frnt = tempRef.getC(i,true);
			String bck = tempRef.getC(i, false);
			returnedVal.addCard(new Card(frnt,bck));
		}
		return returnedVal;
	}
	///Private array to hold all pools
	private Pool[] pools;
	private Pool pool1;
	private Pool pool2;
	private Pool pool3;
	private Pool pool4;
	private Pool pool5;
}

