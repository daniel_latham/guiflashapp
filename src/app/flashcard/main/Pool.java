/*
 * Pool.java
 * 
 * Copyright 2014 Daniel Latham <l3li3l@torguard.tg>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General protected License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General protected License for more details.
 * 
 * You should have received a copy of the GNU General protected License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
package app.flashcard.main;

import java.util.ArrayList;

public class Pool {
	/** Constructor for this pool, calls creatCardsManual if needed, otherwise
	 * the ArrayList is initialized but left empty
	 * @param boolean True if you want to manually input card information
	 * @return void
	 */
	protected Pool()
	{
		//Nothing
	}
	protected String getC(int i, boolean fb) ///fb shorthand for front/back, true for front && false for the back
	{
		if (fb)
		{
			return cards.get(i).getFront();
		}
		else
		{
			return cards.get(i).getBack();
		}
	}
	/**Returns the number of cards in ArrayList
	 * @param void
	 * @return int Size of ArrayList, 0 if no cards
	 */
	protected int getLength()
	{
		return cards.size();
	}
	/** Method for randomly picking card from ArrayList cards
	 * @param void
	 * @return String[] Array with front, back and card number[front, back, cardNumber]
	 */
	protected String[] pickCard()
	{
		if (cards.size() != 0)
		{
			Double Dwhichcard = Math.random()*cards.size();
			int whichcard = Dwhichcard.intValue();
			String front = cards.get(whichcard).getFront();
			String back = cards.get(whichcard).getBack();
			return new String[]{front,back,Integer.toString(whichcard)};
		}
		else
		{
			return new String[]{null,null,null};
		}
	}
	/**Adds card to arraylist
	 * @param Card New Card you want to add
	 * @return void
	 */
	protected void addCard(Card newCard)
	{
		cards.add(newCard);
	}
	/** Removes and returns card from stack.
	 * @param int Index number of card in ArrayList
	 * @return Card Removed card from ArrayList
	 */
	protected Card removeCard(int cardNum)
	{
		Card RnewCard = cards.remove(cardNum);
		cards.trimToSize();
		return RnewCard;
	}
	///Private because I only want this class to access the cards, 
	///so this class got a whole lot of methods becuase of it, making it slightly
	///bloated! Beware!
	private ArrayList<Card> cards = new ArrayList<Card>();
}

