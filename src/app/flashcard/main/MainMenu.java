/**
 * 
 */
package app.flashcard.main;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MainMenu
{
	/**
	 * Constructor for the main menu
	 */
	public MainMenu()
	{
		String importLabel = "Import";
		String saveLabel = "Save";
		String leitnerLabel = "Leitner";
		String drillLabel = "Drill";
		final String[] labels = {importLabel,saveLabel,leitnerLabel,drillLabel};
		JPanel keyPanel = new JPanel();
		keyPanel.setLayout(new GridLayout(2, 2));
		for (int i = 0; i < labels.length; i++)
		{
			final String label = labels[i];
			JButton keyButton = new JButton(label);
			keyButton.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					actions.chooseMenu(label);
				}
			});
			keyPanel.add(keyButton);
		}

		
		JFrame frame = new JFrame();
		Dimension d = new Dimension(300,200);
		frame.setPreferredSize(d);
		frame.setTitle("FlashCard Driller MainMenu");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(keyPanel, BorderLayout.CENTER);
		
		frame.pack();
		frame.setVisible(true);
	}

	/** Sets up the actions object, which is needed for this MainMenu to function properly
	 *
	 * @param actions
	 */
	public void setActionObject(ActionSwing actions)
	{
		this.actions = actions;
	}

	private ActionSwing actions;
}
