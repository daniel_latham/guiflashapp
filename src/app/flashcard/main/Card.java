package app.flashcard.main;

public final class Card {
	///Front of card, can be empty string
	private final String front;
	///Back of card, can be empty string
	private final String back;
	/** Constructor method, just sets the front and
	 * back of this card.
	 * @param String Front of the card you want to initialize
	 * @param String Back of the card you want to initialize
	 */
	protected Card(String frontside, String backside)
	{
		this.front = frontside;
		this.back = backside;
	}
	protected String getFront()
	{
		return this.front;
	}
	protected String getBack()
	{
		return this.back;
	}
}

