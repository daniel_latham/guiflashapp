package app.flashcard.main;
import java.util.regex.*;

import javax.swing.*;

import javax.swing.filechooser.FileNameExtensionFilter;

public class ActionSwing {

	/**
	 * Constructor for this object
	 */
	protected ActionSwing()
	{
		//Nothing
	}
	/**
	 * This takes input from the user and transfers commands over to helper
	 * methods. Currently the Patterns are not needed because the same thing can
	 * be accomplished with the String.compare method
	 * @param label
	 */
	protected void chooseMenu(String label)
	{
		Pattern importP = Pattern.compile("Import");
		Pattern saveP = Pattern.compile("Save");
		Pattern leitnerP = Pattern.compile("Leitner");
		Pattern drillP = Pattern.compile("Drill");
		
		Matcher iMatcher = importP.matcher(label);
		Matcher sMatcher = saveP.matcher(label);
		Matcher leMatcher = leitnerP.matcher(label);
		Matcher dMatcher = drillP.matcher(label);
		if (iMatcher.matches())
		{
			//Gets the current working directory
			final String dir = System.getProperty("user.dir");
			
			JFileChooser chooser = new JFileChooser(dir);
			
			FileNameExtensionFilter filter = new FileNameExtensionFilter(
				"text and txt files", "txt", "text");
			chooser.setFileFilter(filter);
			int returnVal = chooser.showOpenDialog(chooser);
			if(returnVal == JFileChooser.APPROVE_OPTION)
			{
				String filePath = chooser.getSelectedFile().getPath();
				openFile(filePath);
			}
		}
		else if (sMatcher.matches())
		{
			if (this.set != null)
			{
				//Gets the current working directory
				final String dir = System.getProperty("user.dir");
				
				JFileChooser chooser = new JFileChooser(dir);
				
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
						"text and txt files", "txt", "text");
				chooser.setFileFilter(filter);
				int returnVal = chooser.showSaveDialog(chooser);
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{
					String filePath = chooser.getSelectedFile().getPath();
					saveFile(filePath);
				}
			}
			else
			{
				JOptionPane.showMessageDialog(null, "FlashSet is null!", "ERROR", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		else if (leMatcher.matches())
		{
			if (this.set != null)
			{
				leitner();
			}
			else
			{
				JOptionPane.showMessageDialog(null, "FlashSet is null!", "ERROR ", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		else if (dMatcher.matches())
		{
			if (this.set != null)
			{
				drill();
			}
			else 
			{
				JOptionPane.showMessageDialog(null, "FlashSet is null!", "ERROR ", JOptionPane.INFORMATION_MESSAGE);
			}
			
		}
	}
	private void saveFile(String filePath)
	{
		try
		{
			this.set.saveSet(filePath);
			//reopen file
			openFile(filePath);
		}
		catch (Exception e) {}
	}
	/**
	 * Opens up specified filename and sets it as this ActionSwing object's 
	 * flashset, if it fails then this flashset is null
	 * @param filePath
	 */
	private void openFile(String filePath)
	{
		try
		{
			this.set = new FlashSet(filePath);
		}
		catch (Exception e)
		{ /*File does not exist*/
			this.set = null;
		}
		
	}
	/**
	 * Creates a new Frame for the Leitner system
	 */
	private void leitner()
	{
		@SuppressWarnings("unused")
		CardFrame leitnerCardsFrame = new CardFrame("Leitner Mode", 0, this.set);
	}
	/**
	 * Creates a new Frame for the Drill system
	 */
	private void drill()
	{
		@SuppressWarnings("unused")
		CardFrame drillCardsFrame = new CardFrame("Drill Mode", 1, this.set);
	}
	private FlashSet set;
}
