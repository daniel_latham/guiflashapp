Flash card implementation w/ picture && mouse gesture support
@Author Daniel Latham

Import this folder as a project into Eclipse.

To run, execute  Main and import the text file "test.txt" under the "TestCardsSet"
directory, then click leitner or drill, and optionally save when you are finished.



@Important
To import a custom flash card set, your text document must be formatted correctly:

	Line # : Text
	1: Set # 0-4 inclusive
	2: Front string or path to picture
	3: Back string or path to picture
	Repeat

Example:
	Line # : Text
	1: 3
	2: Banana
	3: A tropical fruit
	4: 1
	5: Apple
	6: /home/dan/Downloads/apple.jpg
